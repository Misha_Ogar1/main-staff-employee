import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Consumer;


public class Employee
{
    private String name;
    private Integer salary;
    private Date workStart;

    public Employee(String name, Integer salary, Date workStart)
    {
        this.name = name;
        this.salary = salary;
        this.workStart = workStart;
    }

    public Employee(String name)
    {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public Date getWorkStart() {
        return workStart;
    }

    public void setWorkStart(Date workStart) {
        this.workStart = workStart;
    }

//    public void raiseSalary(Integer byPersent) {
//        int raise = (salary * byPersent) / 100;
//        salary += raise;
//    }
//
//    public void compareTo(Employee other) {
//        System.out.println(Integer.compare(salary, other.salary));
//    }

    public String toString()
    {
        return name + " - " + salary + " - " +
            (new SimpleDateFormat("dd.MM.yyyy")).format(workStart);
    }
}
