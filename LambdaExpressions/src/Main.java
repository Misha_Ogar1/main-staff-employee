import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.Collectors;
//
public class Main
{
    private static String staffFile = "LambdaExpressions/data/staff.txt";
    private static String dateFormat = "dd.MM.yyyy";

    public static void main(String[] args) {
//        ArrayList<Employee> staff = loadStaffFromFile();


        //Maven
        try {
            String data = getDataFromFile("LambdaExpressions/data/staff.json");
            JSONParser parser = new JSONParser();
            JSONArray array = (JSONArray) parser.parse(data);
            for (Object item : array) {
                JSONObject jsonObject = (JSONObject) item;
                System.out.println(jsonObject.get("name"));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

//        //Вывод 5 сотрудников
//        LRUCache<Employee> cache = new LRUCache<>(5);
//        for (Employee employee : staff) {
//            cache.addElement(employee);
//        }
//
//        cache
//                .getAllElement()
//                .forEach(System.out::println);

//                // Создать параллельный стрим из коллекции
//        Stream<String> parallelStream;
//        System.out.println("parallelStream = " + parallelStream.collect(Collectors.toList()));

//        //Сравнение з/п двух работников
//        Employee employee1 = new Employee("Boby");
//        employee1.setSalary(45_000);
//        Employee employee2 = new Employee("Misha");
//        employee2.setSalary(95_000);
//        employee1.compareTo(employee2);
//
//        staff.stream().map(employee -> employee.getSalary()).filter(e -> e >= 100_000).reduce((e1, e2) -> e1 + e2).ifPresent(System.out::println);

//        //метод forEach
//        staff.forEach(employee -> System.out.println(employee));

//        //Сортировка массива по з/п и алфафиту с помощью Comparator
//        staff.sort(Comparator.comparingDouble(Employee::getSalary).thenComparing(Employee::getName));
//        System.out.println(staff);

//        //Вывод макс-й з/п среди работников 2017
//        staff.stream()
//                .filter(e -> e.getWorkStart()
//                        .toInstant()
//                        .atZone(ZoneId.systemDefault())
//                        .toLocalDate().getYear() == 2017)
//                .max(Comparator.comparing(Employee::getSalary))
//                .ifPresent(System.out::println);

//        //Сумма з/п работников, у которых з/п >= 100_000
//        staff.stream()
//                .map(e -> e.getSalary())
//                .filter(s -> s >= 100_000)
//                .reduce((s1, s2) -> s1 + s2)
//                .ifPresent(System.out::println);
//        //Сортировка массива person типа Employee
//        Employee[] person = new Employee[2];
//        person[0] = new Employee("Boris");
//        person[1] = new Employee("Alex");
//
//        Arrays.sort(person, Comparator.comparing(Employee::getName));
//
//        for (Employee e :
//             person) {
//            System.out.println("Name:: -> " + e.getName());
//        }


//        //Сортирока staff по з/п
//        staff.stream().sorted(Comparator.comparing(Employee::getSalary))
//                        .forEach(System.out::println);

//        //Показ максимального значения з/п у staff
//        staff.stream().max(Comparator.comparing(Employee::getSalary))
//                .ifPresent(System.out::println);

//        //Вывод людей с з\п свыше 100_000 руб
//        staff.stream().filter(e -> e.getSalary() >= 100_000).forEach(System.out::println);


//        //Получение чисел, делящихся на 2 без остатка из Stream
//        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
//        numbers.filter(number -> number % 2 == 0).forEach(System.out::println);

//        //Получение чисел, делящихся на 2 без остатка из Массива
//          Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//          Arrays.stream(numbers).filter(n -> n % 2 == 0).forEach(System.out::println);

//        //Бесконечный Stream с помощью метода iterate()
//        Stream.iterate(1, n -> n + 1).forEach(System.out::println);

//        //Бесконечный Stream с помощью метода generate()
//        Stream.generate(() -> " _aaa_ ").forEach(System.out::print);

//        //Stream из строки
//        " !This it Stream! ".chars().forEach(System.out::print);

//        //Сортировка по з/п
//        Collections.sort(staff, Comparator.comparing(Employee::getSalary));

//        //Сортировка по з/п и алфавиту
//        Collections.sort(staff, (o1, o2) -> {
//            if (o1.getSalary().compareTo(o2.getSalary()) == 0) {
//                return o1.getName().compareTo(o2.getName());
//            }
//            return o1.getSalary().compareTo(o2.getSalary());
//        });
//
//        Collections.sort(staff, Comparator.comparing(Employee :: getSalary));


//        //Вывод всех сотрудников
//        for (Employee employee : staff) {
//            System.out.println(employee);
//        }

//        //Вывод сотрудников со старой з/п
//        System.out.println("Old salaries: ");
//        staff.forEach(System.out::println);

//        //Увеличение з/п всем сотрудникам
//        int salaryIncrease = 10_000;
//        staff.forEach(e -> e.setSalary(e.getSalary() + salaryIncrease));

//        //Вывод сотрудников с увеличенной з/п
//        System.out.println("\n" + "New salaries: ");
//        staff.forEach(System.out::println);

    }

    private static ArrayList<Employee> loadStaffFromFile() {
        ArrayList<Employee> staff = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(staffFile));
            for(String line : lines) {
                String[] fragments = line.split("\t");
                if(fragments.length != 3) {
                    System.out.println("Wrong line: " + line);
                    continue;
                }
                staff.add(new Employee(
                    fragments[0],
                    Integer.parseInt(fragments[1]),
                    (new SimpleDateFormat(dateFormat)).parse(fragments[2])
                ));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return staff;
    }

    private static String getDataFromFile(String path) {

        StringBuilder builder = new StringBuilder();

        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            for(String line : lines) {
                builder.append(line);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return builder.toString();
    }
}